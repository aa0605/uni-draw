## 抽奖转盘
本插件精简了某作者约三倍的代码，并封装了动画，直接就可以使用，非常简单，代码简单优雅易读。

除了没封装成组件没其他毛病

## 注
奖品使用字体图标，插件未包含字体，需要换成你自己的字体样式，否则图标看起来应该是乱码

需要使得图片奖励的，可以把字体图标换成图片地址就行了

### 开奖代码示例
```javascript
 let index = 3, duration = 4000
 this.animation(index, duration)

setTimeout(() => {
  uni.showModal({content: this.list[index].isNoPrize ? '抱歉，您未中奖' : '恭喜，中奖'})
  this.btnDisabled = '';
  // document.getElementById('zhuanpano').style=''
}, duration + 1000)
```

## 关于中奖率等行为控制
这些业务逻辑应该由自己写，一般的抽奖业务逻辑示例：1、请求服务器，获取中奖数据。2、将中奖相应的index传入执行转盘

关于需要锁定的功能，可以在抽奖前加一行这个代码：
```javascript
if (this.btnDisabled) return
```
